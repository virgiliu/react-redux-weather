import { FETCH_WEATHER } from '../actions/index';

export default function(state = [], action) {
    switch(action.type)
    {
        case FETCH_WEATHER:
            // Add the new data at the start of existing data
            return [ action.payload.data, ...state ];
    }
    return state;
}
